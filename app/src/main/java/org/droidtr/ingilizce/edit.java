package org.droidtr.ingilizce;

import android.app.*;
import android.os.*;
import android.view.*;
import android.content.*;
import android.widget.*;

public class edit extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit);
		SharedPreferences read = getSharedPreferences("wordlist",MODE_PRIVATE);
		((EditText) findViewById(R.id.et)).setText(read.getString("list","").replaceAll("#€","\n"));
	}
	public void save(View w){
		SharedPreferences.Editor edit = getSharedPreferences("wordlist",MODE_PRIVATE).edit();
		edit.putString("list",((EditText) findViewById(R.id.et)).getText().toString().replaceAll("\n","#€"));
		edit.commit();
		finish();
	}
	public void cancel(View w){
		SharedPreferences read = getSharedPreferences("wordlist",MODE_PRIVATE);
		((EditText) findViewById(R.id.et)).setText(read.getString("list","").replaceAll("#€","\n"));
		finish();
	}
}
