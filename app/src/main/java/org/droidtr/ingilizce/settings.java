package org.droidtr.ingilizce;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.graphics.*;

public class settings extends Activity
{
	String tag="";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		Button ydstb = (Button) findViewById(R.id.ydstb);
		Button customtb = (Button) findViewById(R.id.customtb);
		SharedPreferences read = getSharedPreferences("wordlist",MODE_PRIVATE);
		if(!read.getBoolean("yds",false)){
			ydstb.setBackgroundColor(Color.RED);
		}else{
			ydstb.setBackgroundColor(Color.GREEN);
		}if(!read.getBoolean("custom",true)){
			customtb.setBackgroundColor(Color.RED);
		}else{
			customtb.setBackgroundColor(Color.GREEN);
		}
	}
	public void edit(View w){
		startActivity(new Intent(this,edit.class));
	}
	public void listonoff(View w){
		tag =w.getTag().toString();
		SharedPreferences read = getSharedPreferences("wordlist",MODE_PRIVATE);
		SharedPreferences.Editor edit = getSharedPreferences("wordlist",MODE_PRIVATE).edit();
		edit.putBoolean(tag,!read.getBoolean(tag,false));
		edit.commit();
		onCreate(new Bundle());
	}
}
