package org.droidtr.ingilizce;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.net.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.util.*;
import org.droidtr.ingilizce.*;
import android.media.*;

public class MainActivity extends Activity{
	boolean closeable=false;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
		try{
			IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_ON);
			this.registerReceiver(new bootup(), filter);
			setContentView(R.layout.main);
			DisplayMetrics displayMetrics = new DisplayMetrics();
			WindowManager windowmanager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
			windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
			int deviceWidth = displayMetrics.widthPixels;
			int deviceHeight = displayMetrics.heightPixels;
			int width=0;
			if(deviceWidth>deviceWidth){
				width=deviceWidth;
			}else{
				width=deviceHeight;
			}
			setFinishOnTouchOutside(false);
			TextView te = (TextView) findViewById(R.id.te);
			TextView tt = (TextView) findViewById(R.id.tt);
			LinearLayout ll = (LinearLayout) findViewById(R.id.ll);
			RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl);
			ProgressBar pb = (ProgressBar) findViewById(R.id.actionProgress);
			rl.getLayoutParams().width=(width/2);
			rl.getLayoutParams().height=(width/2);
			ll.getLayoutParams().height=(width/2);
			ll.getLayoutParams().width=(width/2);
			te.setTextSize(width/66);
			tt.setTextSize(width/66);
			pb.getLayoutParams().width=width+20;
			pb.getLayoutParams().height=width+20;
			pb.requestLayout();
			ll.requestLayout();
			rl.requestLayout();
    		next(null);
		}
		catch(Exception e){}
		catch(IOError e){}
		catch(Error e){}
		finally{}
    }
	
	public void close(View w){ 
		finish(); 
	}
	
	public void next(final View w){
		final Handler h = new Handler();
		if(w != null){
			w.setEnabled(false);
			h.postDelayed(new Runnable(){
				public void run(){
					w.setEnabled(true);
				}
			},10);
		}
		min = 60;
		try{
			SharedPreferences read = getSharedPreferences("wordlist",MODE_PRIVATE);
			String customlist =read.getString("list","");
			BufferedReader yds = new BufferedReader(new InputStreamReader(getAssets().open("kelime/liste")));
			TextView te = (TextView) findViewById(R.id.te);
			final TextView tn = (TextView) findViewById(R.id.tn);
			View eq = findViewById(R.id.eq);
			TextView tt = (TextView) findViewById(R.id.tt);
			final TextView ac = (TextView) findViewById(R.id.actionClose);
			final ProgressBar pb = (ProgressBar) findViewById(R.id.actionProgress);
			Random r = new Random();
			StringBuilder sb = new StringBuilder();
			ac.setVisibility(View.INVISIBLE);
			tn.setVisibility(View.INVISIBLE);
			closeable=false;
			setTitle("");
			boolean enable=false;
			if(read.getBoolean("custom",true)){
			for(int i=0;i!=customlist.split("#€").length;i++){
				sb.append(customlist.split("#€")[i]+"#€");
			}
			enable=true;
			}if(read.getBoolean("yds",false)){
			for(int i = 0;yds.ready();i++){
				sb.append(yds.readLine().toString()+"#€");
				}
				enable= true;
			}
			final String[] wordlist = sb.toString().split("#€");
			if (wordlist.length ==0 || !enable){
				te.setText("wordlist not found");
				tt.setText("kelime listesi bulunamadı");
			}else{
			String kelime = wordlist[(new Random()).nextInt(wordlist.length)];
			if (kelime.split("::").length > 1){
				tt.setText(kelime.split("::")[1]);
				te.setText(kelime.split("::")[0]);
			} else next(w);
			}
				int red   = 128-r.nextInt(128);
				int blue  = 128-r.nextInt(128);
				int green = 128-r.nextInt(128);
				findViewById(R.id.ll).setBackground(dialogBg(Color.rgb(127+green,127+blue,127+red),/*ac.getTextSize()*/10000));
				te.setTextColor(Color.rgb(red/2,green/2,blue/2));
				eq.setBackgroundColor(Color.rgb(255-green,255-blue,255-red));
				tt.setTextColor(Color.rgb(red/2,green/2,blue/2));
				tn.setTextColor(Color.rgb(red/2,green/2,blue/2));
				ac.setTextColor(Color.rgb(red/2,green/2,blue/2));
				Drawable d = pb.getProgressDrawable();
				d.setColorFilter(Color.rgb(255-green,255-blue,255-red),PorterDuff.Mode.SRC_ATOP);
				pb.setProgressDrawable(d);
				new Thread(){
					public void run(){
						h.post(new Runnable(){
							int num = 60;
							public void run(){
								min--;
								if(num == 0) close(null);
								if(num ==60-2){
									closeable=true;
										if (wordlist.length !=0){
									tn.setVisibility(View.VISIBLE);
									}
									ac.setVisibility(View.VISIBLE);
									
								}
								if(min < num){
									pb.setProgress(num);
									num --;
									h.postDelayed(this,1000);
								}
							}
						});
					}
				}.start();
			
		} catch(Exception e){}
		catch(IOError e){}
		catch(Error e){}
		finally{}
	}
	
	int min = 60;
	
	public Drawable dialogBg(int color, float radius){
		GradientDrawable gd = new GradientDrawable();
		gd.setColors(new int[]{color,color});
		gd.setCornerRadii(new float[]{radius,radius,radius,radius,radius,radius,radius,radius});
		return gd;
	}

	@Override
	public void onBackPressed(){
		if(closeable){
		close(null);
		super.onBackPressed();
		}
	}

	@Override
	protected void onDestroy()
	{
		if(closeable){
			close(null);
			super.onDestroy();
		}
		
	}

	@Override
	protected void onPause()
	{
		if(closeable){
			close(null);
		super.onPause();
		}
	}
	
}
